package at.fipps.jira.rest;

import javax.xml.bind.annotation.*;
@XmlRootElement(name = "Filesystem")
@XmlAccessorType(XmlAccessType.FIELD)


public class JiraFSModel {

    @XmlElement
    private String name;
	private long total;
	private long used;
	private String percent;

    public JiraFSModel() {}

    public JiraFSModel(String name, long total, long used, String percent) {
        this.name = name;
		this.total =  total;
		this.used = used;
		this.percent = percent;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
	public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total ;
    }
    
    public long getUsed() {
        return used;
    }

    public void setUsed(long used) {
        this.used = used;
    }

	public String getPercent() {
		return percent;
	}

	public void setPercent(String percent) {
		this.percent = percent;
	}
    
    
    
}