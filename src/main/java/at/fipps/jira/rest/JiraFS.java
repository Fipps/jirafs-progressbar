package at.fipps.jira.rest;

import java.io.File;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.util.JiraHome;

/**
 * A resource of message.
 */
@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
@Path("/")
public class JiraFS {

    @GET
    @Path("jirafs")
    public Response getJiraProdFS()
    {
       JiraHome jiraHome = ComponentAccessor.getComponent(JiraHome.class);
	   File folder = new File(jiraHome.getHomePath());
       String name=folder.toString();
       long total=folder.getTotalSpace();
       long free=folder.getUsableSpace();
       long used=total-free;
       double proz= (double) used/total; 
       proz=proz*100;
       String percent = String.format("%.1f",proz);
       return Response.ok(new JiraFSModel(name,total,used, percent)).build();
    }
}
      